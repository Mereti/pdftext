package pl.mereti;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

public class SimpleTable {

    public static final String DEST = "results/tables/simple_table.pdf";

    public static void main(String[] args) throws IOException, DocumentException {
        File file = new File(DEST);
        file.getParentFile().mkdirs();
        new SimpleTable().createPdf(DEST);
    }

    public void createPdf(String dest) throws IOException, DocumentException {
        Document document = new Document();
        PdfWriter.getInstance(document, new FileOutputStream(dest));
        document.open();
        PdfPTable table = new PdfPTable(2);

        PdfPCell cell = new PdfPCell(new Phrase("Resume"));
        cell.setFixedHeight(30);
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setColspan(2);
        table.addCell(cell);

        table.addCell("Imię i nazwisko");
        table.addCell("Kinga Derendal");
        table.addCell("Wyksztalcenie");
        table.addCell("PWSZ Tarnów 2018.11 - do teraz\nWSIZ Rzeszów 2020.11 - do teraz");
        table.addCell("Poznane technologie");
        table.addCell("Java, Spring, Hibernate, Haskel, HTML, CSS, JS, Kotlin, Photoshop, Illustrator, XD");
        table.addCell("Doświadczenie");
        table.addCell("Comp Web Studio");

        document.add(table);
        document.close();
    }

}
